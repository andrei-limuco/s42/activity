// console.log("Hello World")

console.log(document.querySelector("#txt-first-name"))

console.log(document)
//document refers to the whole page
//querySelector is used to select a specific element

console.log(document.getElementById("txt-first-name"))
/*
	Alternative:

	document.getElementById("txt-first-name");
	document.getElementsByClassName()
	document.getElementsByTagName()

*/

const txtFirstName = document.querySelector("#txt-first-name")
const txtLastName = document.querySelector("#txt-last-name")
const spanFullName = document.querySelector("#span-full-name")

//Event Listeners
/*
	selectedElement.addEventListener("event", function);

*/

// txtFirstName.addEventListener("keyup", (event) => {
// 	spanFullName.innerHTML = txtFirstName.value
// })

/*
function printFirstName(event) {
	spanFullName.innerHTML = txtFirstName.value;
}

txtFirstName.addEventListener("keyup", printFirstName)
*/

// txtFirstName.addEventListener("keyup", (event) => {
// 	console.log(event.target)
// 	console.log(event.target.value)
// })

txtFirstName.addEventListener("keyup", printFullName)
txtLastName.addEventListener("keyup", printFullName)

function printFullName(event) {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}

